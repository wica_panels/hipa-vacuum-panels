console.debug( "Executing script in hipa_vacuum_subs.js module...");

export function load_subs( template )
{
    return template
        .replaceAll( '__VACUUM_VALVE_VERT__',                   VACUUM_VALVE_VERT )
        .replaceAll( '__VACUUM_VALVE_HORIZ__',       VACUUM_VALVE_HORIZ )
        .replaceAll( '__VACUUM_PUMP__',              VACUUM_PUMP )
        .replaceAll( '__VACUUM_PUMP_TURBO__',        VACUUM_PUMP_TURBO )
        .replaceAll( '__VACUUM_PUMP_CRYO__',         VACUUM_PUMP_CRYO )
        .replaceAll( '__VACUUM_GAUGE__',             VACUUM_GAUGE )
        .replaceAll( '__VACUUM_BUFFER__',            VACUUM_BUFFER )
        .replaceAll( '__VACUUM_TUBE_VERT__',         VACUUM_TUBE_VERT )
        .replaceAll( '__VACUUM_TUBE_VERT_UPPER__',   VACUUM_TUBE_VERT_UPPER )
        .replaceAll( '__VACUUM_TUBE_VERT_LOWER__',   VACUUM_TUBE_VERT_LOWER )
        .replaceAll( '__VACUUM_TUBE_HORIZ__',        VACUUM_TUBE_HORIZ )
        .replaceAll( '__VACUUM_TUBE_HORIZ_LEFT__',   VACUUM_TUBE_HORIZ_LEFT )
        .replaceAll( '__VACUUM_TUBE_HORIZ_RIGHT__',  VACUUM_TUBE_HORIZ_RIGHT )
        .replaceAll( '__VACUUM_TUBE_T_UPPER__',      VACUUM_TUBE_T_UPPER )
        .replaceAll( '__VACUUM_TUBE_T_LOWER__',      VACUUM_TUBE_T_LOWER )
        .replaceAll( '__VACUUM_TUBE_X__',            VACUUM_TUBE_X )
        .replaceAll( '__VACUUM_TUBE_HEMI_LEFT__',    VACUUM_TUBE_HEMI_LEFT )
        .replaceAll( '__VACUUM_TUBE_HEMI_RIGHT__',   VACUUM_TUBE_HEMI_RIGHT )
        .replaceAll( '__VACUUM_TARGET__',            VACUUM_TARGET )
        .replaceAll( '__VACUUM_MEASUREMENT_POINT__', VACUUM_MEASUREMENT_POINT )
        .replaceAll( '__WATER_FLOW_SENSOR__',        WATER_FLOW_SENSOR )
        .replaceAll( '__WATER_COOLER__',             WATER_COOLER )
        .replaceAll( '__SVG_STYLES__',               SVG_STYLES );
}

const VACUUM_VALVE_VERT =
    `
    <!-- Vacuum Valve: Vertical. Supported vars: 'component_fill_color'. -->
    <symbol id="vacuum_valve_vert" viewBox="0 0 100 100">
      <title>vacuum valve: vertical</title>
      <rect x="22" y="2" rx="12" width="56" height="96" fill="var(--valve_bg_color,lightgray)" stroke="var(--valve_border_color,black)" stroke-width="4"/>
      <path d="M 30 15 H 70 L 30 85 H 70 L 30 15" fill="var(--valve_fg_color,darkgray)"/>
    </symbol>
    `;

const VACUUM_VALVE_HORIZ =
`
    <!-- Vacuum Valve: Horizontal. Supported vars: 'valve_fg_color', 'valve_bg_color', 'valve_border_color. -->
    <symbol id="vacuum_valve_horiz" viewBox="0 0 100 100">
      <title>vacuum valve: horizontal</title>
      <rect x="2" y="22" rx="12" width="96" height="56" fill="var(--valve_bg_color,lightgray)" stroke="var(--valve_border_color,black)" stroke-width="4"/>
      <path d="M 15 30 V 70 L 85 30 V 70 L 15 30" fill="var(--valve_fg_color,darkgray)"/>
    </symbol>
`;

const VACUUM_PUMP =
`
    <!-- Vacuum Pump. Supported vars: 'pump_fg_color', 'pump_bg_color', 'pump_border_color. -->
    <symbol id="vacuum_pump" viewBox="0 0 100 100">
        <title>vacuum pump</title>
        <rect x="2" y="2" rx="12" width="96" height="96" fill="var(--pump_bg_color,lightgray)" stroke="var(--pump_border_color,black)" stroke-width="4"/>
        <circle cx="50" cy="50" r="35" fill="var(--pump_fg_color,darkgray)"/>
        <line x1="45" y1="90" x2="30" y2="10" stroke="lightgray" stroke-width="4"/>
        <line x1="55" y1="90" x2="70" y2="10" stroke="lightgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_PUMP_TURBO =
`
    <!-- Vacuum Turbo Pump. Supported vars: 'pump_fg_color', 'pump_bg_color', 'pump_border_color. -->
    <symbol id="vacuum_pump_turbo" viewBox="0 0 100 100">
      <title>vacuum pump: turbo</title>
      <rect x="2" y="2" rx="12" width="96" height="96" fill="var(--pump_bg_color,lightgray)" stroke="var(--pump_border_color,black)" stroke-width="4"/>
      <circle cx="50" cy="50" r="35" fill="var(--pump_fg_color,darkgray)"/>
      <circle cx="50" cy="50" r="5" fill="var(--pump_bg_color,lightgray)"/>
      <line x1="45" y1="90" x2="30" y2="10" stroke="var(--pump_bg_color,lightgray)" stroke-width="4"/>
      <line x1="55" y1="90" x2="70" y2="10" stroke="var(--pump_bg_color,lightgray)" stroke-width="4"/>
    </symbol>
`;

const VACUUM_PUMP_CRYO =
`
    <!-- Vacuum Cryo Pump. Supported vars: 'component_fill_color'. -->
    <symbol id="vacuum_pump_cryo" viewBox="0 0 100 100">
        <title>vacuum pump: cryo</title>
        <rect x="2" y="2" rx="12" width="96" height="96" fill="var(--pump_bg_color,lightgray)" stroke="var(--pump_border_color,black)" stroke-width="4"/>
        <circle cx="50" cy="50" r="35" fill="var(--pump_fg_color,darkgray)"/>
        <line x1="45" y1="90" x2="30" y2="10" stroke="var(--pump_bg_color,lightgray)" stroke-width="4"/>
        <line x1="55" y1="90" x2="70" y2="10" stroke="var(--pump_bg_color,lightgray)" stroke-width="4"/>
        <circle cx="50" cy="50" r="10" fill="var(--pump_bg_color,lightgray)"/>
        <text x="50" y="54" font-size="13" text-anchor="middle">K</text>
    </symbol>
`;

// noinspection CssUnresolvedCustomProperty
const VACUUM_GAUGE =
`
    <!-- Vacuum Gauge. Supported vars: 'gauge_level_in_percent', 'gauge_scale_1', 'gauge_scale_2', -->
    <!-- 'gauge_bg_color', 'gauge_fg_color', 'gauge_border_color' -->
    <symbol id="vacuum_gauge" viewBox="0 0 100 250">
      <title>vacuum gauge</title>
      <rect x="2" y="2" rx="12" width="96" height="196" fill="var(--gauge_bg_color,lightgray)" stroke="var(--gauge_border_color,black)" stroke-width="4"/>
      <rect x="20" y="30" width="40" height="140" fill="var(--gauge_bg_color,lightgray)" stroke="var(--gauge_border_color,black)" stroke-width="3"/>
      <rect x="22" width="36" fill="var(--gauge_fg_color,mediumseagreen)" style="height: calc( 2px + var(--gauge_level_in_percent) * 1.36px); y:calc( 167px - var(--gauge_level_in_percent) * 1.36px"/>
      <!-- Range Scale Ticks -->
      <line x1="60" y1="40" x2="65" y2="40" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <line x1="60" y1="70" x2="65" y2="70" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <line x1="60" y1="100" x2="65" y2="100" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <line x1="60" y1="130" x2="65" y2="130" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <line x1="60" y1="160" x2="65" y2="160" stroke="var(--gauge_border_color,black)" stroke-width="2"/>
      <!-- Range 1 Scale Labels -->
      <text x="65" y="44" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-3</text>
      <text x="65" y="74" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-4</text>
      <text x="65" y="104" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-5</text>
      <text x="65" y="134" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-6</text>
      <text x="65" y="164" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_1)">10-7</text>
      <!-- Range 2 Scale Labels -->
      <text x="65" y="44" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-0</text>
      <text x="65" y="74" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-1</text>
      <text x="65" y="104" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-2</text>
      <text x="65" y="134" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-3</text>
      <text x="65" y="164" font-weight="bold" font-size="var(--gauge_font_size,14)" style="display:var(--gauge_scale_2)">10-4</text>
    </symbol>
`;

const VACUUM_BUFFER =
`
    <!-- Vacuum Buffer -->
    <symbol id="vacuum_buffer" viewBox="0 0 100 100">
        <title>vacuum buffer</title>
        <rect x="0" y="0" width="100" height="100" fill-opacity="0"/>
        <ellipse cx="50" cy="50" rx="40" ry="10" fill-opacity="0" stroke="darkgray" stroke-width="4"/>
        <line x1="50" y1="0" x2="50" y2="40" stroke="darkgray" stroke-width="4"/>
        <line x1="50" y1="60" x2="50" y2="100" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_VERT =
`
    <!-- Vacuum Tube: Vertical -->
    <symbol id="vacuum_tube_vert" viewBox="0 0 100 100">
        <title>vacuum tube: vertical tube</title>
        <line x1="50" y1="0" x2="50" y2="100" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_VERT_UPPER =
`
    <!-- Vacuum Tube: Vertical Upper -->
    <symbol id="vacuum_tube_vert_upper" viewBox="0 0 100 100">
        <title>vacuum tube: vertical upper</title>
        <line x1="50" y1="0" x2="50" y2="50" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_VERT_LOWER =
`
    <!-- Vacuum Tube: Vertical Lower -->
    <symbol id="vacuum_tube_vert_lower" viewBox="0 0 100 100">
        <title>vacuum tube: vertical lower</title>
        <line x1="50" y1="50" x2="50" y2="100" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_HORIZ =
`
    <!-- Vacuum Tube: Horizontal -->
    <symbol id="vacuum_tube_horiz" viewBox="0 0 100 100">
        <title>vacuum tube: horizontal</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_HORIZ_LEFT =
`
    <!-- Vacuum Tube: Horizontal Left -->
    <symbol id="vacuum_tube_horiz_left" viewBox="0 0 100 100">
        <title>vacuum tube: horizontal left</title>
        <line x1="0" y1="50" x2="50" y2="50" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_HORIZ_RIGHT =
`
    <!-- Vacuum Tube: Horizontal Right -->
    <symbol id="vacuum_tube_horiz_right" viewBox="0 0 100 100">
        <title>vacuum tube: horizontal right</title>
        <line x1="50" y1="50" x2="100" y2="50" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_T_UPPER =
`
    <!-- Vacuum Tube: T-Section Upper-->
    <symbol id="vacuum_tube_t_upper" viewBox="0 0 100 100">
        <title>vacuum tube: T-section upper</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="darkgray" stroke-width="4"/>
        <line x1="50" y1="0" x2="50" y2="50" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_T_LOWER =
`
    <!-- Vacuum Tube: T-Section Lower-->
    <symbol id="vacuum_tube_t_lower" viewBox="0 0 100 100">
        <title>vacuum tube: T-section lower</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="darkgray" stroke-width="4"/>
        <line x1="50" y1="50" x2="50" y2="100" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_X =
`
    <!-- Vacuum Tube: X-Section -->
    <symbol id="vacuum_tube_x" viewBox="0 0 100 100">
        <title>vacuum tube: X-section</title>
        <line x1="0" y1="50" x2="100" y2="50" stroke="darkgray" stroke-width="4"/>
        <line x1="50" y1="0" x2="50" y2="100" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_HEMI_LEFT =
`
    <!-- Vacuum Tube: Hemisphere Left -->
    <symbol id="vacuum_tube_hemi_left" viewBox="0 0 100 100" overflow="visible">
        <title>vacuum tube: hemisphere left</title>
        <path d="M 100 0 A 50 50 0 0 0 100 100" fill-opacity="0" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TUBE_HEMI_RIGHT =
`
    <!-- Vacuum Tube: Hemisphere Right -->
    <symbol id="vacuum_tube_hemi_right" viewBox="0 0 100 100" overflow="visible">
        <title>vacuum tube: hemisphere right</title>
        <path d="M 0 0 A 50 50 0 0 1 0 100" fill-opacity="0" stroke="darkgray" stroke-width="4"/>
    </symbol>
`;

const VACUUM_TARGET =
    `
    <!-- Vacuum Target -->
    <symbol id="vacuum_target" viewBox="0 0 100 100">
      <title>vacuum target</title>
      <rect x="22" y="2" rx="12" width="56" height="96" fill="var(--valve_bg_color,lightgray)" stroke="var(--valve_border_color,black)" stroke-width="4"/>
      <ellipse cx="50" cy="50" rx="10" ry="40" fill-opacity="0" stroke="darkgray" stroke-width="4"/>
    </symbol>
    `;

const VACUUM_MEASUREMENT_POINT =
    `
    <!-- Vacuum Measurement Point. Supported vars: 'meas_fg_color', 'meas_bg_color', 'meas_border_color. -->
    <symbol id="vacuum_measurement_point" viewBox="0 0 50 50">
        <title>vacuum measurement point</title>
        <rect x="2" y="2" rx="6" width="46" height="46" fill="var(--meas_bg_color,lightgray)" stroke="var(--meas_border_color,black)" stroke-width="4"/>
        <circle cx="25" cy="25" r="17" fill="var(--meas_fg_color,darkgray)"/>
        <line x1="22" y1="45" x2="15" y2="5" stroke="lightgray" stroke-width="4"/>
        <line x1="28" y1="45" x2="35" y2="5" stroke="lightgray" stroke-width="4"/>
        <line x1="7" y1="25" x2="43" y2="25" stroke="lightgray" stroke-width="4"/>
    </symbol>
`;

const WATER_FLOW_SENSOR =
    `
    <!-- Water Flow Sensor. Supported vars: 'flow_sensor_fg_color', 'flow_sensor_bg_color', 'flow_sensor_border_color. -->
    <symbol id="water_flow_sensor" viewBox="0 0 50 50">
        <title>water flow sensor</title>
        <rect x="2" y="2" rx="6" width="46" height="46" fill="var(--flow_sensor_bg_color,lightgray)" stroke="var(--flow_sensor_border_color,black)" stroke-width="4"/>
        <circle cx="25" cy="25" r="17" fill="var(--flow_sensor_fg_color,darkgray)"/>
        <rect x="15" y="20" rx="2" width="20" height="12" fill="var(--flow_sensor_bg_color,lightgray)" stroke-width="4"/>
    </symbol>
`;

const WATER_COOLER =
    `
    <!-- Water Cooler.  Supported vars: 'water_cooler_bg_color', 'water_cooler_border_color. -->
    <symbol id="water_cooler" viewBox="0 0 100 100">
        <title>water cooler</title>
        <rect x="12" y="12" rx="12" width="76" height="76" fill="var(--water_cooler_bg_color,lightgray)" stroke="var(--water_cooler_border_color,black)" stroke-width="4"/>
        <text x="50" y="54" font-size="18" text-anchor="middle">TZK400</text>
    </symbol>
`;

const SVG_STYLES =
    `
    :root {
       --pump_fg_color: black;
       --pump_bg_color: lightgray;
       --pump_border_color: black;
       --valve_fg_color: black;
       --valve_bg_color: lightgray;
       --valve_border_color: black;
       --gauge_fg_color: black;
       --gauge_bg_color: lightgray;
       --gauge_border_color: black;
       --gauge_font_size: 14px;
       --tube_stroke_color: darkslategrey;
    }
     
    .nav_box {
        fill: lightgray;
        stroke: darkgray;
        stroke-width:4;
        rx: 8;
    }
    
    .nav_label {
        font: bold 24px sans-serif;
        fill: darkblue;
    }
    
    .cavity {
       fill: royalblue;
       stroke: darkgray;
       stroke-width:4;
    }
    
    .info_box {
        fill: lightgray;
        stroke: darkgray;
        stroke-width:4;
    }
    
    .tube {
        stroke: darkgray;
        stroke-width: 5px;
    }
    
    .banner {
        font: bold 42px sans-serif;
        fill: darkblue;
    }
    
    .label_l {
        font: bold 32px sans-serif;
        fill: darkblue;
    }
    
    .label_m {
        font: bold 24px sans-serif;
        fill: darkblue;
    }
    
    .value {
        font: 24px sans-serif;
        fill: blue;
    }
    
    a:link {
        cursor: pointer;
    }   
`;
