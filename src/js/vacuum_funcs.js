console.debug( "Executing script in vacuum_funcs.js module...");


const ILLEGAL_TEXT = "ILLEGAL";
const ILLEGAL_COLOR = "white";

const OFFLINE_TEXT = "OFFLINE";
const OFFLINE_COLOR = "white";

const WARNING_COLOR = "lightcoral";
const OK_COLOR = "mediumseagreen";

let svgDoc = null;

export function init_svg_document_ref()
{
    if ( svgDoc === null ) {
        const svgElement = document.getElementById( "svgHostElement" );
        svgDoc = svgElement.contentDocument;
    }
}

export function update_svg_numeric_value( event, svgEleId, fractionDigits, optUnits = false, optExp = false )
{
    init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

    if ( ! verify_connection_and_channel( event, () => targetElement.textContent = "ERR" ) )
    {
        return;
    }

    const value = optExp ? event.channelValueLatest[ "val" ].toExponential( fractionDigits ):  event.channelValueLatest.val.toFixed( fractionDigits );
    const units = optUnits ? event.channelMetadata[ "egu" ] : "";
    targetElement.textContent = value + " " + units;
}

export function update_svg_string_value( event, svgEleId, optUnits = false )
{
    init_svg_document_ref();
    const targetElement = svgDoc.getElementById( svgEleId );
    if ( targetElement == null ) {
        return;
    }

   if ( ! verify_connection_and_channel( event, () => targetElement.textContent = "ERR" ) )
   {
       return;
   }

    const units = optUnits ? event.channelMetadata[ "egu" ] : "";
    targetElement.textContent = event.channelValueLatest[ "val" ] + " " + units ;
}

// SVG Component Text Update support
export function update_svg_turbo_pump_text( event, svgElementId  )
{
    const turboPumpTextMappings = new Map( [ [0, "UNDEF"], [1, "FAULT"], [2, "STOPPED"], [3, "STARTING"], [4, "STARTED"], [5, "STOPPING"] ] ) ;
    update_svg_component_text( event, svgElementId, turboPumpTextMappings );
}

export function update_svg_kryo_pump_text( event, svgElementId  )
{
    const kryoPumpTextMappings = new Map( [ [0, "UNDEF"], [1, "FAULT"], [2, "STOPPED"], [3, "STARTING"], [4, "STARTED"], [5, "STOPPING"] ] ) ;
    update_svg_component_text( event, svgElementId, kryoPumpTextMappings );
}

export function update_svg_pump_text( event, svgElementId  )
{
    const pumpTextMappings = new Map( [ [0, "UNDEF"], [1, "FAULT"], [2, "STOPPED"], [3, "STARTING"], [4, "STARTED"], [5, "STOPPING"] ] ) ;
    update_svg_component_text( event, svgElementId, pumpTextMappings );
}

export function update_svg_valve_text( event, svgElementId  )
{
    const valveTextMappings = new Map( [ [0, "UNDEF"], [1, "CLOSED"], [2, "OPEN"], [3, "MOVING"], [4, "CLOSED/LOCKED"], [5, "REDUCED/OPEN"] ] ) ;
    update_svg_component_text( event, svgElementId, valveTextMappings );
}

export function update_svg_flow_sensor_text( event, svgElementId  )
{
    const flowSensorTextTextMappings = new Map( [ [ "OFF", "Off"], [ "ON", "On"] ] ) ;
    update_svg_component_text( event, svgElementId, flowSensorTextTextMappings );
}


// SVG Component Color Update support
export function update_svg_turbo_pump_color( event, svgElementId  )
{
    const PUMP_UNDEF_COLOR     = "darkmagenta";
    const PUMP_FAULT_COLOR     = "lightcoral";
    const PUMP_STOPPED_COLOR   = "darkgray";
    const PUMP_STARTING_COLOR  = "yellow";
    const PUMP_STARTED_COLOR   = "mediumseagreen";
    const PUMP_STOPPING_COLOR  = "lightgray";
    const pumpColorMappings = new Map( [ [0, PUMP_UNDEF_COLOR], [1, PUMP_FAULT_COLOR], [2, PUMP_STOPPED_COLOR], [3, PUMP_STARTING_COLOR],
                                                             [4, PUMP_STARTED_COLOR], [5, PUMP_STOPPING_COLOR] ] ) ;
    update_svg_component_color_new( event, svgElementId, "--pump_fg_color", pumpColorMappings );
}

export function update_svg_kryo_pump_color( event, svgElementId  )
{
    const PUMP_UNDEF_COLOR     = "darkmagenta";
    const PUMP_FAULT_COLOR     = "lightcoral";
    const PUMP_STOPPED_COLOR   = "darkgray";
    const PUMP_STARTING_COLOR  = "yellow";
    const PUMP_STARTED_COLOR   = "mediumseagreen";
    const PUMP_STOPPING_COLOR  = "lightgray";
    const PUMP_COOLING_COLOR   = "deepskyblue";
    const PUMP_REGENERATING_COLOR  = "magenta";
    const pumpColorMappings = new Map( [ [0, PUMP_UNDEF_COLOR], [1, PUMP_FAULT_COLOR], [2, PUMP_STOPPED_COLOR], [3, PUMP_STARTING_COLOR],
                                                             [4, PUMP_STARTED_COLOR], [5, PUMP_STOPPING_COLOR], [6, PUMP_COOLING_COLOR], [7, PUMP_REGENERATING_COLOR] ] ) ;
    update_svg_component_color_new( event, svgElementId, "--pump_fg_color", pumpColorMappings );
}

export function update_svg_pump_color(event, svgElementId  )
{
    const PUMP_UNDEF_COLOR     = "darkmagenta";
    const PUMP_FAULT_COLOR     = "lightcoral";
    const PUMP_STOPPED_COLOR   = "darkgray";
    const PUMP_STARTING_COLOR  = "yellow";
    const PUMP_STARTED_COLOR   = "mediumseagreen";
    const PUMP_STOPPING_COLOR  = "lightgray";
    const pumpColorMappings = new Map( [ [0, PUMP_UNDEF_COLOR], [1, PUMP_FAULT_COLOR], [2, PUMP_STOPPED_COLOR], [3, PUMP_STARTING_COLOR],
                                                             [4, PUMP_STARTED_COLOR], [5, PUMP_STOPPING_COLOR] ] ) ;
    update_svg_component_color_new( event, svgElementId, "--pump_fg_color", pumpColorMappings );
}

export function update_svg_valve_color( event, svgElementId  )
{
    const VALVE_UNDEF_COLOR    = "darkmagenta";
    const VALVE_CLOSED_COLOR   = "lightcoral";
    const VALVE_OPEN_COLOR     = "mediumseagreen";
    const VALVE_MOVING_COLOR   = "yellow";
    const VALVE_LOCKED_COLOR   = "red";
    const VALVE_REDUCED_COLOR  = "limegreen";
    const VALVE_ENABLED_COLOR  = "springgreen";
    const valveColorMappings = new Map( [ [0, VALVE_UNDEF_COLOR], [1, VALVE_CLOSED_COLOR], [2, VALVE_OPEN_COLOR], [3, VALVE_MOVING_COLOR],
                                                              [4, VALVE_LOCKED_COLOR], [5, VALVE_REDUCED_COLOR], [6, VALVE_ENABLED_COLOR] ] ) ;
    update_svg_component_color_new( event, svgElementId, "--valve_fg_color", valveColorMappings );
}

export function update_svg_flow_sensor_color( event, svgElementId  )
{
    const FLOW_SENSOR_OFF_COLOR= "red";
    const FLOW_SENSOR_ON_COLOR = "mediumseagreen";
    const flowSensorColorMappings = new Map( [ [ "OFF", FLOW_SENSOR_OFF_COLOR], [ "ON", FLOW_SENSOR_ON_COLOR] ] ) ;
    update_svg_component_color_new( event, svgElementId, "--flow_sensor_fg_color", flowSensorColorMappings );
}

// Lower level support
function update_svg_component_color_new( event, svgElementId, property, mappings  )
{
    function handleErrors()
    {
        svgElement.style.setProperty( property, OFFLINE_COLOR );
    }
    init_svg_document_ref();
    const svgElement = svgDoc.getElementById( svgElementId );
    if ( svgElement == null ) {
        return;
    }
    if ( ! verify_connection_and_channel( event, () => handleErrors() ) )
    {
        return;
    }
    const currentValue = event.channelValueLatest[ "val" ];
    const componentColor = mappings.has( currentValue ) ? mappings.get( currentValue ): ILLEGAL_COLOR;
    svgElement.style.setProperty( property, componentColor );
}

export function update_svg_component_text( event, svgElementId, mappings  )
{
    function handleErrors()
    {
        svgElement.textContent = OFFLINE_TEXT;
    }
    init_svg_document_ref();
    const svgElement = svgDoc.getElementById( svgElementId );
    if ( svgElement == null ) {
        return;
    }
    if ( ! verify_connection_and_channel( event, () => handleErrors() ) )
    {
        return;
    }
    const currentValue = event.channelValueLatest[ "val" ];
    svgElement.textContent = mappings.has( currentValue ) ? mappings.get( currentValue ): ILLEGAL_TEXT;
}

export function update_svg_gauge( event, svgElementId, optSelectSensitiveScale = true )
{
    init_svg_document_ref();
    const svgElement = svgDoc.getElementById( svgElementId );
    if ( svgElement == null ) {
        return;
    }

    if ( ! verify_connection_and_channel( event, () => {
        svgElement.style.setProperty( "--gauge_fg_color", OFFLINE_COLOR );
        svgElement.style.setProperty( "--gauge_bg_color", OFFLINE_COLOR );
    } ) )
    {
        return;
    }

    const DEFAULT_BG_COLOR = "lightgray";
    svgElement.style.setProperty( "--gauge_bg_color", DEFAULT_BG_COLOR );

    const value =  event.channelValueLatest[ "val" ];
    // Range Scale 1 Calculation is derived as follows:
    // - Pressure = 10-3:  gauge_level_in_percent = 95%
    // - Pressure = 10-7:  gauge_level_in_percent = 5%

    // Range Scale 2 Calculation is derived as follows:
    // - Pressure = 10-0:  gauge_level_in_percent = 95%
    // - Pressure = 10-5:  gauge_level_in_percent = 5%

    // noinspection PointlessArithmeticExpressionJS
    const gauge_level_in_percent = optSelectSensitiveScale ?
        clamp(95 + 22.5 * (3 + Math.log10(value)), 0, 100) :
        clamp(95 + 22.5 * (0 + Math.log10(value)), 0, 100);

    // Colourise the gauge depending on whether inside or outside limits
    if ( gauge_level_in_percent >= 100 )
    {
        svgElement.style.setProperty( "--gauge_fg_color", WARNING_COLOR );
    }
    else
    {
        svgElement.style.setProperty( "--gauge_fg_color", OK_COLOR );
    }

    // Set the level of the gauge
    svgElement.style.setProperty( "--gauge_level_in_percent", gauge_level_in_percent );
}

function clamp( num, min, max ) {
    return num <= min ? min : num >= max ? max : num;
}


function verify_connection_and_channel( event, callbackHandler )
{
    const wicaStreamState = event.target.dataset.wicaStreamState;
    const wicaChannelConnectionState = event.target.dataset.wicaChannelConnectionState;

    // The wicaStreamState attribute looks typically like this: 'opened-<n>', where <n> is the connection attempt.
    if ( !wicaStreamState.includes( "opened" ) ) {
        callbackHandler();
        return false;
    }

    // The wicaChannelConnectionState attribute looks like this when the underlying channel is connected: 'connected'.
    if  ( wicaChannelConnectionState !== "connected" ) {
        callbackHandler();
        return false;
    }
    return true;
}
