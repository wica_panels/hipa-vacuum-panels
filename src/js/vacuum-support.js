console.debug( "Executing script in vacuum-support.js module...");

import {DocumentSupportLoader} from "@psi/wica-js/client-api";
import * as VACUUM_FUNCS from "./vacuum_funcs.js"

export {VACUUM_FUNCS};

let documentSupportLoader = null;

// Provide a hook for restarting wica support of the current document
function restartDocumentSupportLoader() {

    if ( documentSupportLoader === null ) {
        console.log( "Wica is creating new DocumentSupportLoader..." );
        const WICA_OWN_HOST = location.origin;
        const cssSupportRequired = false;
        const textRendererSupportRequired = false;
        documentSupportLoader = new DocumentSupportLoader( WICA_OWN_HOST, cssSupportRequired, textRendererSupportRequired );
        console.log( "Wica DocumentSupportLoader was created OK." );
    }
    else {
        console.log( "Wica is shutting down support for the existing document..." );
        documentSupportLoader.shutdown();
        console.log( "Wica document support was shutdown OK." );
    }

    console.log( "Wica is activating support for a new document..." );

    setTimeout( () => {
        documentSupportLoader.activate( Number.MAX_VALUE, 500 );
        console.log("Wica document support has been activated OK.");
    }, 0 );
}

document.wicaRestartDocumentSupportLoader = restartDocumentSupportLoader;

window.onload = function() {
    setTimeout( () => {
        // Suppress document test rendering
        document.wicaRestartDocumentSupportLoader();
        console.log("Wica document support has been activated OK.");
    }, 0 );
}