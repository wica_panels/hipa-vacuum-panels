# Overview

This log describes the functionality of tagged versions within the repository.

# Tags  

## [Version 1.0.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.0.0)
  Initial release.

## [Version 1.1.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.1.0)

### Description
- Cleaned up the project structure ready for future enhancements.

### Git Commit Information (the most important ones)

- Eliminated local installation of wica; will now take from node_modules area.
- Add support for project tooling directory.
- Upgrade node dependencies to latest.
- Upgrade html files to use new tooling.
- Eliminate redundant file.
- Upgrade project tooling.
- Create release 1.1.0
- Fixed typo.

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Release Date
* 2022-08-05


## [Version 1.2.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.2.0)

### Description
- CHORE: Project restructured to eliminate repetition by using template substitution principle adopted elsewhere.
- ENHANCEMENT: Now uses same symbols and styling resources as trialled at PROSCAN.

### Git Commit Information (the most important ones)
- Copy symbol resources from PROSCAN vacuum project.
- Copy functions from PROSCAN vacuum project.
- Eliminate old files, replaced by definitions in subs file.
- Moved to separate JS folder.
- Create SVG template files.
- Create release 1.2.0
- Now uses copy plugin (which offers possibility to do template substitutions).
- Change __STYLES__ to __SVG_STYLES__.
- Eliminate IntelliJ warnings.
- BUG FIX: Fixed gauge scales.

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Release Date
* 2022-08-06


## [Version 1.3.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.3.0)

### Description
- Add support for IP2 vacuum section.

### Git Commit Information (the most important ones)
- Add VACUUM_TARGET symbol (needed for IP2).
- Add support for IP2 vacuum section.
- Add support for run_all scripts.
- Add support for IP2 vacuum section.
- Removed superfluous text (typo)
- Create release 1.3.0

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The vacuum gauges do not go red if out of range. Need to discuss with vacuum group how 
  best to implement this.

### Release Date
* 2022-08-08


## [Version 1.4.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.4.0)

### Description
- Add support for IW2 vacuum section.
- Add improved support for connection handling => panels visually reflect if the underlying data sources are offline.
- Add support for gauge out of limit colourisation => gauge goes red when greater than 100%. 
- Other minor panel improvements.

### Git Commit Information (the most important ones)
- INJ2: Adjust width to better fit on page.
- IP2: Adjust width to better fit on page.
- IP2: Eliminate unneeded text labels.
- IP2: Set default time to NC.
- CW: Set default time to NC.
- 860keV: Set default time to NC.
- IW2: Add initial support.
- INJ2: Set default text labels to NC.
- Create release 1.4.0
- Change update_svg_gauge_level -> update_svg_gauge to reflect the fact that this function now performs limit colourisation.
- Add fallback default color for --gauge_fg_color variable for consistency with other symbols.
- Improve comments.
- Add support for connection handling.
- Add support for gauge colourisation.

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The vacuum gauges do not go red if out of range. Need to discuss with vacuum group how
  best to implement this.
- The symbols go orange when not connected which can lead to confusion.

### Release Date
* 2022-08-20


## [Version 1.5.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.5.0)

### Description
- Apply changes requested by Stefan Fleischli in email of 2022-09-29.

### Git Commit Information (the most important ones)
- BUG FIX: Eliminate redundant link.
- ENHANCEMENT: Improve position of vacuum gauges.
- ENHANCEMENT: Add support for showing PWK0 temperature.
- ENHANCEMENT: Add comment to README saying how to publish changes.
- BUG FIX: Add support for missing PIT3 section.
- BUG FIX: Correct duplicated text labels.

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The vacuum gauges do not go red if out of range. Need to discuss with vacuum group how
  best to implement this.
- The symbols go orange when not connected which can lead to confusion.

### Release Date
* 2022-09-29


## [Version 1.6.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.6.0)

### Description
- Adjusted CW and 60keV panels to provide initial support for new PLC system values.

### Git Commit Information (the most important ones)
- ENHANCEMENT: add support for new PLC system. Needs review and testing. Colors and symbols
  probably still need further revision. 

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The vacuum gauges do not go red if out of range. Need to discuss with vacuum group how
  best to implement this.
- The symbols go orange when not connected which can lead to confusion.

### Release Date
* 2023-03-18


## [Version 1.7.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.7.0)

### Description
- This version updates the CW page using the latest information from the equivalent caQtDm page.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add support for new channel reporting on state of TKZ cryo pump.
- ENHANCEMENT: Add support for reporting on PV2 HV and BV valve states.
- CHORE: Adjust viewport to make room for extra PV2 components.

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The vacuum gauges do not go red if out of range. Need to discuss with vacuum group how
  best to implement this.
- The symbols go orange when not connected which can lead to confusion.

### Release Date
* 2024-02-26


## [Version 1.8.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.8.0)

### Description
- This version updates the CW page following feedback from the vacuum group. 
- See the JIRA issue https://jira.psi.ch/browse/CTRLIT-9224

### Git Commit Information (the most important ones)
- BUG FIX: Reduce precision to one decimal place.
- ENHANCEMENT: Add support for new measurement point symbol.
- ENHANCEMENT: Add initial support for H20 cryo pump.
- Create release 1.8.0

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The vacuum gauges do not go red if out of range. Need to discuss with vacuum group how
  best to implement this.

### Release Date
* 2024-04-16


## [Version 1.9.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.9.0)

### Description
- This version updates the 860KeV page following feedback from the vacuum group.
- See the JIRA issue https://jira.psi.ch/browse/CTRLIT-9224

### Git Commit Information (the most important ones)
- CHORE: Adapt 860keV drawing to new situation.
- CHORE: Tweak measuring point positions.
- CHORE: Create release 1.9.0

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The vacuum gauges do not go red if out of range. Need to discuss with vacuum group how
  best to implement this.

### Release Date
* 2024-04-17


## [Version 1.10.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.10.0)

### Description
- This version adds better support for the water colling system on the CW page.

### Git Commit Information (the most important ones)
- ENHANCEMENT: Add support for water flow sensor symbol and text.
- ENHANCEMENT: Add support for water flow sensor and water cooler symbols.
- BUG FIX: Fix vacuum cryo pum symbol curved edges.
- CHORE: Improve name.
- CHORE: Update dependencies.
- CHORE: Update format to more modern 'es' syntax.
- CHORE: Add .js to import to avoid lint error.
- CHORE: Update eslint config file to more modern approach.
- CHORE: Explicitly state that project uses es modules.
- CHORE: Update package-lock file.
- BUG FIX: Fix regression bug with PV2 vacuum pump.
- ENHANCEMENT: Create release 1.10.0

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The vacuum gauges do not go red if out of range. Need to discuss with vacuum group how
  best to implement this.

### Release Date
* 2024-04-22


## [Version 1.10.1](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.10.1)

### Description
- Fixed bug reported by Stefan.

### Git Commit Information (the most important ones)
- BUG FIX: Adjust VVD1 labels.
- CHORE: Add package lock file.
- BUG FIX: Reverse GK and GV labels as reported by Stefan Fleischli.
- ENHANCEMENT: Create release 1.10.1

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- Not all the panels have been converted to the new PLC format.

### Release Date
* 2024-04-23


## [Version 1.11.0](https://gitlab.psi.ch/wica_panels/gfa-ilk-panels/tags/1.11.0)

### Description
- Completed conversion of all existing Wica panels to the new PLC format. Still to do: update 
  layouts for IP2, IW2, Injector2 for consistency with latest caQtDm layouts.

### Git Commit Information (the most important ones)
- CHORE: Formatting change only.
- CHORE: Improve name consistency with text functions: 'update_svg_vaku_pump_color' -> 'update_svg_pump_color'.
- ENHANCEMENT: Update Injector2 to work with new EPICS channels.
- ENHANCEMENT: Update IP2 to work with new EPICS channels.
- BUG FIX: Fix typo in vacuum target title (shows on tooltip).
- ENHANCEMENT: Update IW2 to work with new EPICS channels.
- CHORE: Retire old component update function (now all panels have been converted to new format).
- ENHANCEMENT: Update README to show that panels are now created directly inside the Jetbrains IDE using the built-in SVG editor.
- ENHANCEMENT: Create release 1.10.1

### Dependencies:
- Works with [wica-js](https://github.com/paulscherrerinstitute/wica-js) V1.5.4

### Known Bugs
- The panels IP2, IW2, Injector2 still need reworking for consistency with the caQtDm panels.

### Release Date
* 2024-04-23
