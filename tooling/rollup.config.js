import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import copy from "rollup-plugin-copy";
import {load_subs} from "../src/subs/hipa_vacuum_subs.js";

// noinspection JSUnusedGlobalSymbols
export default () => {

    const buildTarget = process.env.BUILD_DEV ? "build/dev" :
                        process.env.BUILD_PROD ? "build/prod" :
                        process.env.BUILD_EXT ? "build/ext" : "";

    return [
        {
            input: 'src/js/vacuum-support.js',
            output: {
                dir:  buildTarget,
                format: 'es',
                sourcemap: true,
            },
            plugins: [
                resolve(),
                commonjs(),
                copy( {
                    targets: [
                        // Install CW
                        { src: "src/html/cw.html", dest: buildTarget },
                        {
                            src: "src/templates/cw.template.svg", dest: buildTarget,
                            transform: (contents) => load_subs( contents.toString() ),
                            rename: "cw.svg",
                        },
                        // Install 860KeV
                        { src: "src/html/860KeV.html", dest: buildTarget },
                        {
                            src: "src/templates/860KeV.template.svg", dest: buildTarget,
                            transform: (contents) => load_subs( contents.toString() ),
                            rename: "860KeV.svg",
                        },
                        // Install Injector2
                        { src: "src/html/injector2.html", dest: buildTarget },
                        {
                            src: "src/templates/injector2.template.svg", dest: buildTarget,
                            transform: (contents) => load_subs( contents.toString() ),
                            rename: "injector2.svg",
                        },
                        // Install IP2
                        { src: "src/html/ip2.html", dest: buildTarget },
                        {
                            src: "src/templates/ip2.template.svg", dest: buildTarget,
                            transform: (contents) => load_subs( contents.toString() ),
                            rename: "ip2.svg",
                        },
                        // Install IW2
                        { src: "src/html/iw2.html", dest: buildTarget },
                        {
                            src: "src/templates/iw2.template.svg", dest: buildTarget,
                            transform: (contents) => load_subs( contents.toString() ),
                            rename: "iw2.svg",
                        }
                    ],
                })
            ]
        }
    ]
}
