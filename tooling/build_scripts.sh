#!/bin/bash

# Script starts here. Script is intended to be sourced and used like this:
# source build_scripts.sh && dist_publish_all

# Exit immediately if anything unexpected happens.
set -e

# Enable tracing for debug if required
#set -x

# Sets up a local worktree named 'dist_xxx' to track the remote branch named 'dist_xxx'.
function __dist_worktree_create() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     git worktree add -f "$1" "$1"
  else
     echo "Sets up a local worktree named 'dist_xxx' to track the remote branch named 'dist_xxx'."
  fi
}

# Pulls the latest information from the remote branch named 'dist_xxx' to update the local worktree named 'dist_xxx'.
function __dist_worktree_pull() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     cd "$1"
     git pull
  else
     echo "Pulls the latest information from the remote branch named 'dist_xxx' to update the local worktree named 'dist_xxx'."
  fi
}

# Assembles the data to be published for the local worktree named 'dist_xxx'.
function __dist_assemble() {
  if [[ "$1" == "dist_dev" ]]; then
     cd "$1"
     rm -fr ./*
     cp -R ../build/dev/* .
     cd ..
  elif [[ "$1" == "dist_prod" ]]; then
     cd "$1"
     rm -fr ./*
     cp -R ../build/prod/* .
     cd ..
  elif [[ "$1" == "dist_ext" ]]; then
     cd "$1"
     rm -fr ./*
     cp -R ../build/ext/* .
     cd ..
  else
     echo "Copies the current state of the local build to the local branch named 'dist_xxx'."
  fi
}

# Commits the current state of the local worktree named 'dist_xxx' to the local branch named 'dist_xxx'.
function __dist_worktree_commit() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     cd "$1"
     git add .
     git diff-index --quiet HEAD || git commit -m "updates" .
     cd ..
  else
     echo "Commits the current state of the local worktree named 'dist_xxx' to the local branch named 'dist_xxx'."
  fi
}

# Pushes the latest information from the local worktree named 'dist_xxx' to update the remote branch named 'dist_xxx'.
function __dist_worktree_push() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     cd "$1"
     git push
     cd ..
  else
     echo "Pushes the latest information from the local worktree named 'dist_xxx' to update the remote branch named 'dist_xxx'."
  fi
}

# Trigger autodeployment on the autodeployment server associated with the supplied dist area."
function __dist_server_deploy() {
  contentType="Content-Type: application/x-www-form-urlencoded";
  deployCommand="deploy?gitUrl=git@gitlab.psi.ch:wica_panels/hipa-vacuum-panels.git";
  devServer="https://gfa-wica-dev.psi.ch:8443"
  prodServer="https://gfa-wica.psi.ch:8443"
  extServer="https://wica.psi.ch:8443"
  if [[ "$1" == "dist_dev" ]]; then
     curl -H "${contentType}" -X POST "${devServer}/${deployCommand}";
  elif [[ "$1" == "dist_prod" ]]; then
     curl -H "${contentType}" -X POST "${prodServer}/${deployCommand}";
  elif [[ "$1" == "dist_ext" ]]; then
     curl -H "${contentType}" -X POST "${extServer}/${deployCommand}";
  else
     echo "Trigger autodeployment on the autodeployment server associated with the supplied dist area."
  fi
}

# Commits the current state of the local worktree named 'dist_xxx' and pushes it to the remote branch named 'dist_xxx'.
function dist_publish() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     __dist_assemble "$1"
     __dist_worktree_commit "$1"
     __dist_worktree_push "$1"
     __dist_server_deploy "$1"
  else
     echo "Commits the current state of the local worktree named 'dist_xxx' and pushes it to the remote branch named 'dist_xxx'."
  fi
}

# Deletes and removes the local worktree and branch named 'dist_xxx'.
function dist_clean() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     rm -fr "$1"
     git worktree remove "$1"
     git branch -D "$1"
  else
     echo "Deletes and removes the local worktree and branch named 'dist_xxx'."
  fi
}

#  Creates the local worktrees ('dist_dev', 'dist_prod' and 'dist_ext').
function dist_create_all() {
  __dist_worktree_create "dist_dev"
  __dist_worktree_create "dist_prod"
  __dist_worktree_create "dist_ext"
}

# Publishes the local worktrees ('dist_dev', 'dist_prod' and 'dist_ext') to their corresponding remote branches.
function dist_publish_all() {
  dist_publish "dist_dev"
  dist_publish "dist_prod"
  dist_publish "dist_ext"
}

#  Deletes and removed the local worktrees ('dist_dev', 'dist_prod' and 'dist_ext').
function dist_clean_all() {
  dist_clean "dist_dev"
  dist_clean "dist_prod"
  dist_clean "dist_ext"
}
