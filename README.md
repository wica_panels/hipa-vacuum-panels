Note: this documentation is still under construction !!

# Overview

This repository provide support for visually monitoring the evolving state of HIPA's vacuum systems in real time from locations outside of PSI.

The solution is based on a new technology called Wica which has been developed by PSI's Controls Section. Wica is an open source project available on [GitHub](https://github.com/paulscherrerinstitute/wica-http).

PSI's Wica installation provides on-demand http streaming access to the EPICS channels in PSI's machine control system. This project leverages off these information streams to update the state of various widgets that have been incorporated into SVG Drawings of the vacuum schematics.

# Getting Started

The following procedure is applicable for Desktop PC's running either Mac OSX, Linux or Windows.

First you will need to install the following resources on your system.

1. Node Installation. Available [here.](https://nodejs.org/en/download/)
1. Boxy SVG Editor. Available [here.](https://boxy-svg.com/)

Node is open source and available free of charge. 
Boxy is available via the webstores on the various platforms. There is a small charge.

# Creating a new panel - the "Old Way". No longer supported

On linux or mac platform the installation procedure is as shown below: 

(On a Windows platform you will need to use the equivalent commands).

1. Clone this repository onto your local file system
   ```
   git clone git@git.psi.ch:wica_panels/hipa-vacuum-panels.git
   ```
   
1. Go into the projects 'src' directory and run the web server that is provided as part of the project's node installation.
   ```
   cd hipa-vacuum-panels/src
   npx http-server
   ```
   
1. Make a copy of the SVG and HTML vacuum template files.
   ```
   cp vacuum_template.svg <my_new_panel.svg> 
   cp vacuum_template.html <my_new_panel.html> 
   ```

1. The drawing should now be visible in your web browser if you navigate to the following URL:
   ```
   http://localhost:8080/my_new_panel.html
   ```
   If all is well the time field should be updating in real time.
   
1. Edit the SVG Drawing to pull on to it the symbols available on the DEFS panel of the editor. When you are happy with your drawing save the file.
   
1. The next step is to defines variables on the fields of your drawing that you wish to update in real time. The project currently supports the following:
   - updating text fields
   - updating the color of the widgets
   - updating the level of the vacuum gauges.


# Creating a new panel - the "New Way"

Nowadays, (2024-04-23) the SVG support in the IntelliJ IDE is good enough that one can edit the SVG files directly in 
the IDE. For this reason the old way of (the built-in http server which was previously bundled in the projects has 
now been retired).


# Publishing panels

1. Use the npm 'dev', 'prod', or 'ext' script targets to automatically deploy your changes to the relevant wica servers.